/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wutinan.shapeproject;

/**
 *
 * @author INGK
 */
public class Square {
    private double x;
    
    public Square(double x){
        this.x = x;
    }
    public double calSquare(){
     return x * x;
    }
    public double getx(){
        return x;
    }
    public void set(double x){
        if(x <= 0 ){
            System.out.println("Error:x must more than zero!!!");
            return;
        }
        this.x = x;
    }
}
