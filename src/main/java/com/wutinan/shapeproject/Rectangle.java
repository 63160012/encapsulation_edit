/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wutinan.shapeproject;

/**
 *
 * @author INGK
 */
public class Rectangle {
    private double w;
    private double h;
    public Rectangle(double w,double h){
        this.w = w;
        this.h = h;
    }
    public double calRectangle(){
     return w * h ;
    }
    public double getw(){
        return w;
    }
    public double geth(){
        return h;
    }   
        
    public void set(double w , double h){
        if(w <= 0 ){
            System.out.println("Error:wide why must more than zero!!!");
            return;
        }
        if(h <= 0){
            System.out.println("Error:high must more than zero!!!");
            return;
        }
        this.w = w;
        this.h = h;
    }
    
    
}
