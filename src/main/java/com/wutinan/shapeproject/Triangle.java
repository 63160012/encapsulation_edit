/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wutinan.shapeproject;

/**
 *
 * @author INGK
 */
public class Triangle {
    private double x;
    private double y;
    private double z = 0.5;
    public Triangle (double x,double y){
        this.x = x;
        this.y = y;
    }
    public double calTriangle(){
     return x * y * z;
    }
    public double getw(){
        return x;
    }
    public double geth(){
        return y;
    }   
        
    public void set(double x , double y){
        if(x <= 0 ){
            System.out.println("Error:wide why must more than zero!!!");
            return;
        }
        if(y <= 0){
            System.out.println("Error:high must more than zero!!!");
            return;
        }
        this.x = x;
        this.y = y;
    }
}
